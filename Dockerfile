FROM node:14.7-alpine as base

# Create app directory
WORKDIR /app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package.json ./


RUN apk add --update curl tini && \
    yarn --prod

COPY src/ .

EXPOSE 3000
ENTRYPOINT  [ "/sbin/tini", "--"]
CMD ["node" ,"server.js" ]

ARG CI_COMMIT_SHA=000
ENV COMMIT=${CI_COMMIT_SHA}

